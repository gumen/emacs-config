;; -*- mode: emacs-lisp; lexical-binding: t -*-
(setq-default js-indent-level 2)
(setq-default js-jsx-indent-level js-indent-level)
(setq-default css-indent-offset 2)
(setq-default truncate-lines t)         ; Don't do that.  Your making a mess.
(setq-default make-backup-files nil)
(setq-default create-lockfiles nil)
(setq automatic-hscrolling 'current-line)
(setq isearch-lazy-count t)
(setq calendar-week-start-day 1)
(setq confirm-kill-emacs 'yes-or-no-p)  ; Don't kill me, please. - Palpatine
(setq find-name-arg "-iname")		; find-name-dired case insensitive
(set-language-environment "UTF-8")      ; Avoid other encoding
(fset 'yes-or-no-p 'y-or-n-p)
(load-file "~/.config/emacs/gopher.el/gopher.el") ; gopher-mode

;;; minor modes
(global-auto-revert-mode t)    ; Reload file after change
(pending-delete-mode t)        ; Remove selected region while typing

;;; javascript js-jsx
(defun my/js-mode-on-hook ()
  (setq-local indent-tabs-mode nil))

(eval-after-load "js"
  (lambda ()
    (add-hook 'js-mode-hook 'my/js-mode-on-hook)
    (require 'sgml-mode)
    (define-key js-mode-map     (kbd "C-c C-e") 'sgml-close-tag)
    (define-key js-jsx-mode-map (kbd "C-c C-e") 'sgml-close-tag)
    (define-key js-jsx-mode-map (kbd "C-c C-f") 'sgml-skip-tag-forward)
    (define-key js-jsx-mode-map (kbd "C-c C-b") 'sgml-skip-tag-backward)))

;;; completion
(setq read-buffer-completion-ignore-case t)
(setq completion-ignore-case t)         ; For `project-find-file'

;;; compilation
(setq compilation-disable-input t)  ; Close compilation window using q
(global-set-key (kbd "<f6>") 'recompile)

;;; grep
(require 'grep)
(add-to-list 'grep-find-ignored-directories "node_modules")

;;; vc (for `project-find-file')
(require 'vc-hooks)
(add-to-list 'vc-directory-exclusion-list "node_modules")

;; commit setup
(defun my/setup-vc-git-log-edit-mode ()
  (ispell-change-dictionary "en")
  (flyspell-mode 1))

(add-hook 'log-edit-mode-hook 'my/setup-vc-git-log-edit-mode)

;;; flymake
(require 'flymake)
(define-key flymake-mode-map (kbd "M-n") 'flymake-goto-next-error)
(define-key flymake-mode-map (kbd "M-p") 'flymake-goto-prev-error)

;;; eww
;; Overwrite shr functions to avoid raised text
(eval-after-load "eww"
  (lambda ()
    (defun shr-tag-sup (dom) (let ((start (point))) (shr-generic dom)))
    (defun shr-tag-sub (dom) (let ((start (point))) (shr-generic dom)))))

;; Avoid very long lines of text
(defun my/set-buffer-max-shr-width (&optional max-width)
  "Set value of `shr-width' in current buffer.  If buffer is
smaller then given MAX-WIDTH then set nil.  Default MAX-WIDTH
value is 70."
  (let ((max-width (or max-width 70)))
    (setq-local shr-width
      (if (< max-width (window-width)) max-width nil))))

(add-hook 'eww-mode-hook 'my/set-buffer-max-shr-width)

;;; spellcheck toggle (flyspell & ispell)
(defun my/spellcheck-toggle ()
  (interactive)
  (flyspell-prog-mode)
  (ispell-change-dictionary
   (if (string= ispell-local-dictionary "en") "polish" "en") nil))

(global-set-key (kbd "<f8>") 'my/spellcheck-toggle)

;;;; org-mode
(require 'org)
(setq org-use-speed-commands t)         ; Be quick or be dead!
(setq org-agenda-restore-windows-after-quit t)
(setq-default org-list-description-max-indent 1)

(org-babel-do-load-languages 'org-babel-load-languages '((shell . t)))

(global-set-key (kbd "C-c a") 'org-agenda)
(global-set-key (kbd "C-c k") 'org-capture)

(eval-after-load "org" '(require 'ox-md nil t)) ; Enable markdown export
(eval-after-load "org" '(require 'ox-gfm nil t)) ; Enable git markdown export

;;;; rcirc irc https://xkcd.com/1782/
(require 'rcirc)
(eval-after-load 'rcirc '(require 'rcirc-color))
(eval-after-load 'rcirc '(require 'rcirc-styles))

(setq rcirc-kill-channel-buffers t)
(setq rcirc-server-alist
      '(("irc.freenode.net" :port 6697 :encryption "tls" :nick "irek"
	 :user-name "gum3n" :channels ("#emacs" "#emacsconf" "##c"
				       "##javascript" "#bender"))))

(defun rcirc-on-hook ()
  (setq truncate-lines nil)
  (ispell-change-dictionary "en")
  (flyspell-mode 1)
  (rcirc-omit-mode 1)
  (setq-local fill-column 80)
  (rcirc-styles-activate))

(add-hook 'rcirc-mode-hook 'rcirc-on-hook)
(global-set-key (kbd "C-c r") 'rcirc-menu)

;;; put stuff
(put 'narrow-to-region 'disabled nil)
(put 'scroll-left 'disabled nil)
(put 'dired-find-alternate-file 'disabled nil)

;;;; package
(setq package-archives
  '(("gnu" . "https://elpa.gnu.org/packages/")
    ("melpa" . "https://melpa.org/packages/")))

;;;; mail
(require 'rmail)
(setq rmail-file-name "~/rmail/inbox")
(setq rmail-primary-inbox-list (list "imaps://mail%40gumen.pl@wn02.webd.pl"))
(setq rmail-movemail-variant-in-use 'mailutils)
(setq rmail-remote-password-required t)
(setq rmail-default-file "~/rmail/archive")
(setq rmail-delete-after-output t)

(defun my/rmail-commit-push ()
    (if (= 0 (call-process "git" nil nil nil "commit" "-am" "update"))
      (async-shell-command "git push origin master")))
  
(add-hook 'rmail-quit-hook 'my/rmail-commit-push)
(add-hook 'rmail-after-save-hook 'my/rmail-commit-push)

;;; SMTP (send mail with M-x m)
(setq message-send-mail-function 'message-smtpmail-send-it)
(setq mail-user-agent 'message-user-agent)

(setq user-mail-address "mail@gumen.pl")
(setq smtpmail-smtp-server "wn02.webd.pl")
(setq smtpmail-stream-type 'ssl)
(setq smtpmail-smtp-service 465)	    ; Port
(setq smtpmail-smtp-user user-mail-address) ; In my case same as mail address
(setq message-default-headers (concat "BCC: " user-mail-address "\n"))
(setq mail-personal-alias-file "~/files/mail")

(defun my/message-mode-setup ()
  (ispell-change-dictionary "polish")
  (flyspell-mode 1))

(add-hook 'message-mode-hook 'my/message-mode-setup)


;;;; WIP get mails from ~/files/contacts.org
(let ((buffer (find-file-noselect "~/files/contacts.org" t)))
  (with-current-buffer buffer
    (beginning-of-buffer)
    (let ((point-start  (search-forward-regexp "^ *:MAIL.*: *"))
	  (point-end    (search-forward-regexp "$"))
	  (point-header (search-backward-regexp "^\*+ .*")))
      point-header
      ;; (window-point)
      ;; (set-bu-point point-end)
      (point)
    )))


;;;; changing (toggle) font size
(defvar my/font-size 10.0)

(defun my/font-size-toggle ()
  (interactive)
  (setq my/font-size (if (>= my/font-size 14.0) 10.0
		       (+ my/font-size 2.0)))
  (set-frame-font (font-spec :family "Monospace" :weight 'normal
			     :slant 'normal :size my/font-size)))

(global-set-key (kbd "<f7>") 'my/font-size-toggle)
