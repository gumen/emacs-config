;; -*- lexical-binding: t -*-

;;; look and feel
(menu-bar-mode -1)
(tool-bar-mode -1)
(scroll-bar-mode -1)

;; customize
(setq custom-file (concat user-emacs-directory "custom-file.el"))
